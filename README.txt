CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
-------------
WordCount allows user to view word counts stats for each user: total words, total nodes, total comments, average words per node, etc.

REQUIREMENTS
-------------
No special requirements.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules for further information.

CONFIGURATION
-------------
The module has no modifiable settings. There is no configuration. 

MAINTAINERS
-----------
 * Krzysztof Krawczyk - https://www.drupal.org/u/pllvsh